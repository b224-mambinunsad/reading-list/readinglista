// console.log("Hello World!");

// #1
function makeAlpahbetical(word){
	console.log(`This string is NOT Alphabetical Order:  
${word}`)
	let splitWord = word.split("");
	let sortWord = splitWord.sort();
	let joinwWord = sortWord.join("")	
	return console.log(`This string is Alphabetical Order: 
${joinwWord}`);
};

makeAlpahbetical("webmaster");


// #2
function getVowel(word){
	console.log(`Get the numbers of vowel in: 
${word}`)
	let inputWord = word;
	let vowelWord = "";

	for(let i = 0; i < inputWord.length; i++){
		if(
			inputWord[i].toLowerCase() == "a" ||
			inputWord[i].toLowerCase() == "e" ||
			inputWord[i].toLowerCase() == "i" ||
			inputWord[i].toLowerCase() == "o" ||
			inputWord[i].toLowerCase() == "u" 
		){
			vowelWord += inputWord[i];
		};
	};
	return console.log(`The Number of vowels is: 
${vowelWord.length}`);
};


getVowel("The quick brown fox");


// #3
let countries = ["Indonesia", "Vietnam", "Thailand", "Philippines"];
console.log(`List of countries in array:
${countries}`);

function insertAndSortArr(word){
	countries.unshift(word);
	countries.sort();
	return console.log(`Added "${word}" in list of countries and sorted it`);
};


insertAndSortArr("China");
console.log(countries);


// #4

let mySelf = {
	firstName: "Earl John",
	lastName: "Mambinunsad",
	age: 27,
	gender: "Male",
	nationality : "Filipino"
};

console.log(mySelf);

function person(firstName,lastName,age,gender,nationality){
	this.firstName = firstName
	this.lastName = lastName
	this.age = age
	this. gender = gender
	this.nationality = nationality
	this.whoAmI = function(){
		console.log(`I am a ${this.nationality} and my name is ${this.firstName} ${this.lastName} age ${this.age} and im a ${this.gender}`)
	}
};

let person1 = new person("John", "Doe", 22, "Male", "American");
let person2 = new person("Jane", "Doe", 25, "Female", "Australian");
let person3 = new person("Will", "Smith", 34, "Male", "African American");

console.log(person1.whoAmI());
console.log(person2.whoAmI());
console.log(person3.whoAmI());


